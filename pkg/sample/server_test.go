package sample

import (
	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"
)

func TestUserIndexHandler(t *testing.T) {
	router := NewRouter()

	req := httptest.NewRequest(http.MethodGet, "/users", nil)
	rec := httptest.NewRecorder()

	router.ServeHTTP(rec, req)

	assert.Equal(t, http.StatusOK, rec.Code)
	assert.JSONEq(t,
		`[{"name": "taro", "email": "taro@example.com"}, 
                   {"name": "jiro", "email": "jiro@example.com"}]`,
		rec.Body.String())
}

func TestUserShowHandler1(t *testing.T) {
	router := NewRouter()

	req := httptest.NewRequest(http.MethodGet, "/users/taro", nil)
	rec := httptest.NewRecorder()

	router.ServeHTTP(rec, req)

	assert.Equal(t, http.StatusOK, rec.Code)
	assert.JSONEq(t,
		`{"name": "taro", "email": "taro@example.com"}`,
		rec.Body.String())
}

func TestUserShowHandler2(t *testing.T) {
	router := NewRouter()

	req := httptest.NewRequest(http.MethodGet, "/users/saburo", nil)
	rec := httptest.NewRecorder()

	router.ServeHTTP(rec, req)

	assert.Equal(t, http.StatusNotFound, rec.Code)
	assert.JSONEq(t, `{"name": "", "email": ""}`, rec.Body.String())
}

func TestUserCreateHadnler(t *testing.T) {
	router := NewRouter()

	form := make(url.Values)
	form.Set("name", "Saburo")
	form.Set("email", "saburo@example.com")
	req := httptest.NewRequest(http.MethodPost, "/users", strings.NewReader(form.Encode()))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationForm)
	rec := httptest.NewRecorder()

	router.ServeHTTP(rec, req)
	assert.Equal(t, http.StatusCreated, rec.Code)
	assert.JSONEq(t,
		`{"name": "Saburo", "email": "saburo@example.com"}`,
		rec.Body.String())
}

func TestUserCreateHadnlerWithError(t *testing.T) {
	router := NewRouter()

	form := make(url.Values)
	form.Set("name", "Saburo")
	form.Set("email", "saburo@example.com")
	req := httptest.NewRequest(http.MethodPost, "/users", strings.NewReader(form.Encode()))
	//req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationForm)
	rec := httptest.NewRecorder()

	router.ServeHTTP(rec, req)
	assert.Equal(t, http.StatusUnsupportedMediaType, rec.Code)
	assert.JSONEq(t,
		`{"message": "Unsupported Media Type"}`,
		rec.Body.String())
}
